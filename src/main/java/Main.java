import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * Gets all devices.
     * This method is not thread-safe.
     *
     * @return a list of PcapNetworkInterfaces.
     * @throws Exception if an error occurs in the pcap native library.
     */
    public static void findAllDevs() throws Exception {
        PointerByReference alldevsPP = new PointerByReference();
        NativeMappings.PcapErrbuf errbuf = new NativeMappings.PcapErrbuf();

        int rc = NativeMappings.pcap_findalldevs(alldevsPP, errbuf);
        if (rc != 0) {
            StringBuilder sb = new StringBuilder(50);
            sb.append("Return code: ")
                    .append(rc)
                    .append(", Message: ")
                    .append(errbuf);
            throw new Exception(sb.toString());
        }
        if (errbuf.length() != 0) {
            logger.warn("{}", errbuf);
        }

        Pointer alldevsp = alldevsPP.getValue();
        if (alldevsp == null) {
            logger.info("No NIF was found.");
            return;
        }

        NativeMappings.pcap_if pcapIf = new NativeMappings.pcap_if(alldevsp);

        int nifs = 0;
        for (NativeMappings.pcap_if pif = pcapIf; pif != null; pif = pif.next) {
            logger.info("Device: " + pif);
            nifs += 1;
        }

        NativeMappings.pcap_freealldevs(pcapIf.getPointer());

        logger.info("{} NIF(s) found.", nifs);
        return;
    }

    public static void main(String[] args) throws IOException {
        try{
            findAllDevs();
        }
        catch(Exception ex){
            logger.error("Exception :" + ex.getMessage());
        }
    }
}
